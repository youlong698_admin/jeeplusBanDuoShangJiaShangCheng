/**
 * 初始化通知详情对话框
 */
var TCardInfoDlg = {
    tCardInfoData : {}
};

/**
 * 清除数据
 */
TCardInfoDlg.clearData = function() {
    this.tCardInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TCardInfoDlg.set = function(key, val) {
    this.tCardInfoData[key] = (typeof value == "undefined") ? $("#" + key).val() : value;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TCardInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
TCardInfoDlg.close = function() {
    parent.layer.close(window.parent.TCard.layerIndex);
}

/**
 * 收集数据
 */
TCardInfoDlg.collectData = function() {
    this.set('id').set('storeOwer').set('storeAddress');
}

/**
 * 提交添加
 */
TCardInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tCard/add", function(data){
        Feng.success("添加成功!");
        window.parent.TCard.table.refresh();
        TCardInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tCardInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
TCardInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/tCard/update", function(data){
        Feng.success("修改成功!");
        window.parent.TCard.table.refresh();
        TCardInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.tCardInfoData);
    ajax.start();
}

$(function() {

});
