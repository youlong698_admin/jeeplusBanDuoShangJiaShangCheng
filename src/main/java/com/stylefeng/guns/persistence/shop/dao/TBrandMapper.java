package com.stylefeng.guns.persistence.shop.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.persistence.shop.model.TBrand;
import com.stylefeng.guns.persistence.shop.model.TFloor;

/**
 * <p>
  * 商品类型表 Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface TBrandMapper extends BaseMapper<TBrand> {

}