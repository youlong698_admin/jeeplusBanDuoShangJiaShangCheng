package com.stylefeng.guns.persistence.shop.dao;

import com.stylefeng.guns.persistence.shop.model.TOrderLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 订单处理历史表 Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface TOrderLogMapper extends BaseMapper<TOrderLog> {

}