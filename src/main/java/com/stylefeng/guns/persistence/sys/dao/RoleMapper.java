package com.stylefeng.guns.persistence.sys.dao;

import com.stylefeng.guns.persistence.sys.model.Role;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-16
 */
public interface RoleMapper extends BaseMapper<Role> {

}