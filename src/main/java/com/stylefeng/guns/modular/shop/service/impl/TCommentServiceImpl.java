package com.stylefeng.guns.modular.shop.service.impl;

import com.stylefeng.guns.persistence.shop.model.TComment;
import com.stylefeng.guns.persistence.shop.dao.TCommentMapper;
import com.stylefeng.guns.modular.shop.service.ITCommentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Service
public class TCommentServiceImpl extends ServiceImpl<TCommentMapper, TComment> implements ITCommentService {
	
}
