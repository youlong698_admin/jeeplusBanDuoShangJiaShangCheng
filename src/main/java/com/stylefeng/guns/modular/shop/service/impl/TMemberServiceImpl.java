package com.stylefeng.guns.modular.shop.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.util.PasswordEncoder;
import com.stylefeng.guns.persistence.shop.model.TMember;
import com.stylefeng.guns.persistence.shop.dao.TMemberMapper;
import com.stylefeng.guns.modular.shop.service.ITMemberService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.web.wx.WxConfigProperties;
import com.stylefeng.web.wx.service.impl.WeixinService;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Service
public class TMemberServiceImpl extends ServiceImpl<TMemberMapper, TMember> implements ITMemberService {

    @Resource
    TMemberMapper tMemberMapper;
    @Override
    public TMember checkUser(String username, String password) {
        TMember sysUser = new TMember();
        String secPwd = PasswordEncoder.encrypt(password, username);
        sysUser.setUsername(username);
        sysUser.setPassword(secPwd);
        //	sysUser.setDelFlag(Constant.DEL_FLAG_NORMAL);
        sysUser.setStatus(1);
        TMember users = tMemberMapper.selectOne(sysUser);
        if(users != null) {
            return users;
        }
        return null;
    }

    /**
     * 合并微信用户信息
     *
     * @param wxOauthCode
     * @return
     */
    @Override
    public TMember mergeUserInfo(String wxOauthCode) {
        return null;
    }

    /**
     * 获取微信用户
     *
     * @param wxOauthCode
     * @return
     */
    @Override
    public TMember getByWxOauthCode(String wxOauthCode) throws WxErrorException {
        WxMpInMemoryConfigStorage configStorage = new WxMpInMemoryConfigStorage();
        configStorage.setAppId(WxConfigProperties.appid);
        configStorage.setSecret(WxConfigProperties.appsecret);
        configStorage.setToken(WxConfigProperties.token);
        configStorage.setAesKey(WxConfigProperties.aeskey);
        WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(configStorage);
        WxMpOAuth2AccessToken accessToken = wxMpService.oauth2getAccessToken(wxOauthCode);
        WxMpUser wxMpUser = wxMpService.oauth2getUserInfo(accessToken, "zh_CN");

        TMember user = toPromotionUser(wxMpUser);

        TMember user2 = getByWxOpenId(user.getWxopenid());

        return null == user2 ? user : user2;
    }

    private TMember toPromotionUser(WxMpUser wxMpUser) {
        TMember user = new TMember();
        user.setWxavatar(wxMpUser.getHeadImgUrl());
        user.setWxnickname(wxMpUser.getNickname());
        user.setWxopenid(wxMpUser.getOpenId());
        user.setWxunionid(wxMpUser.getUnionId());
        return user;
    }


    /**
     * 根据 微信openid查询
     *
     * @param openid
     * @return
     */
    @Override
    public TMember getByWxOpenId(String openid) {
        TMember promotionUser = new TMember();
        promotionUser.setWxopenid(openid);
        return this.selectOne(new EntityWrapper<>(promotionUser));
    }
}
